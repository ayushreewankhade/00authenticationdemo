package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.demo.filter.JwtRequestFilter;
import com.example.demo.service.CustomUserDetailsService;
import com.example.demo.util.AuthEntryPoint;

//@EnableWebSecurity
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Config extends WebSecurityConfigurerAdapter {

	 @Autowired
	 CustomUserDetailsService customUserDetailsService;
	 
	 @Autowired
	 JwtRequestFilter jwtRequestFilter;
	 
	 @Autowired
	 AuthEntryPoint authEntryPoint;

	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        super.configure(auth);
	        auth.userDetailsService(customUserDetailsService);
	      
	    }

	    @Override
	    public void configure(HttpSecurity security) throws Exception {
	        security.csrf().disable()
	                .authorizeRequests()
	                .antMatchers("/authenticate", "/signin", "/signup", "/forgot","/reset-password","/jobs-applied/{pageNo}/{pageSize}","/signout","/jobs").permitAll()
	                .anyRequest().authenticated().and().exceptionHandling().authenticationEntryPoint(authEntryPoint).and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	        security.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	    }

	    @Bean
	    public PasswordEncoder passwordEncoder(){
	        return new BCryptPasswordEncoder();
	    }

	    @Override
	    @Bean
	    protected AuthenticationManager authenticationManager() throws Exception {
	        return super.authenticationManager();
	    }
}