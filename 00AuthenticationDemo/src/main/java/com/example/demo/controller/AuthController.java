package com.example.demo.controller;

import java.util.Calendar;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ApiResponse;
import com.example.demo.dto.AuthenticationRequest;
import com.example.demo.dto.AuthenticationResponse;

import com.example.demo.dto.JobApplicationDto;
import com.example.demo.dto.LoggerDto;
import com.example.demo.dto.LoginRequest;
import com.example.demo.dto.SignUpRequest;
import com.example.demo.dto.SuccessResponseDto;
import com.example.demo.dto.ResetPasswordDto;
import com.example.demo.exception.ApiExceptions;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ErrorDetails;
import com.example.demo.exception.ErrorResponseDto;

import com.example.demo.model.Job;
import com.example.demo.model.JobApplication;
import com.example.demo.model.JwtUser;
import com.example.demo.repo.JobApplicationRepo;
import com.example.demo.repo.JobRepo;
import com.example.demo.repo.JwtUserRepository;
import com.example.demo.service.CustomUserDetailsService;

import com.example.demo.service.JobApplicationService;
import com.example.demo.service.JobService;
import com.example.demo.service.LoggerService;
import com.example.demo.util.JwtUtil;
import com.example.demo.util.PasswordValidator;

import io.jsonwebtoken.ExpiredJwtException;



@RestController
//@RequestMapping("/user")
public class AuthController {

	 @Autowired
	    AuthenticationManager authenticationManager;

	    @Autowired
	    PasswordEncoder passwordEncoder;

	    @Autowired
	    CustomUserDetailsService userDetailsService;

	    @Autowired
	    JwtUtil jwtUtil;
	    
	    @Autowired
	    JobService jobService;

	    @Autowired
	    JwtUserRepository userRepository;
	    
	    @Autowired
	    JobApplicationService jobApplicationService;
	    
	    @Autowired
	    LoggerService loggerService;
	    
	    @Autowired
	    JobRepo jobRepo;
	    
	    

	    
	    @GetMapping("/checkUser")
	    public String checkUser(){
	        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        String currentPrincipalName = authentication.getName();
	        return  currentPrincipalName;
	    }

	    /*@PostMapping("/authenticate")
	    public ResponseEntity<?> authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
	        Authentication authenticate = authenticationManager.authenticate(
	                new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
	        );

	        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

	        final String jwt = jwtUtil.generateToken(userDetails);

	        return ResponseEntity.ok(new AuthenticationResponse(jwt));

	    }*/

	    @PostMapping("/signin")
	    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

	        /*Authentication authentication = authenticationManager.authenticate(
	                new UsernamePasswordAuthenticationToken(
	                        loginRequest.getUsernameOrEmail(),
	                        loginRequest.getPassword()
	                        
	                )
	        );*/
	    	try {
	    	JwtUser user = userDetailsService.FindByEmail(loginRequest.getUsernameOrEmail());   //findByEmail(authenticationRequest.getEmail());
			System.out.println(user.getPassword());

			if (userDetailsService.comparePassword(loginRequest.getPassword(), user.getPassword())) {
	       // SecurityContextHolder.getContext().setAuthentication(authentication);
	       // UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequest.getUsernameOrEmail());

	        String jwt = jwtUtil.generateToken(user);

	        LoggerDto logger = new LoggerDto(); //
			logger.setToken(jwt);
			Calendar calender = Calendar.getInstance();
			calender.add(Calendar.HOUR_OF_DAY, 5);
			logger.setExpireAt(calender.getTime());
			loggerService.createLogger(logger, user); //
			return ResponseEntity.ok(new AuthenticationResponse(jwt));
	    }
			}catch(BadCredentialsException e) {
			throw new ApiExceptions("INVALID USERNAME OR PASSWORD");

		}catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"User Not Found"),HttpStatus.NOT_FOUND);

		}
		return new ResponseEntity<>(new ErrorResponseDto("Invalid PASSWORD", "CHECK PASSWORD"),HttpStatus.UNAUTHORIZED);
	    }
	    @PostMapping("/signup")
	    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
 
	    	//if(userRepository.findUserByEmail(signUpRequest.getPassword().matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8,20}$")) != false) {
	    	//	 return new ResponseEntity(new ApiResponse(false, "Enter Strong password"),
		    //               HttpStatus.BAD_REQUEST);
	    	//}
	    	String email = signUpRequest.getEmail();
			String password = signUpRequest.getPassword();

	    	if (PasswordValidator.isValid(password)) {
	        if(userRepository.findUserByEmail(signUpRequest.getEmail()) != null) {
	          return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
	                   HttpStatus.BAD_REQUEST); 
	        }
	        // Creating user's account
	        JwtUser jwtUser = new JwtUser();
	        jwtUser.setEmail(signUpRequest.getEmail());
	        jwtUser.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
	        userRepository.save(jwtUser);
	        return ResponseEntity.ok(new ApiResponse(true, "User registered successfully"));
	    	}	 else  {

				return ResponseEntity.ok(new ErrorResponseDto(
						"Password should have Minimum 8 and maximum 50 characters, at least one uppercase letter, one lowercase letter, one number and one special character and No White Spaces",
						"Password validation not done"));
	    	} 	
	       
}
 
//	    @PostMapping("/forgot-password")
//		public String forgotPassword(@RequestParam String email) {
//
//			String response = userDetailsService.forgotPassword(email);
//
//			if (!response.startsWith("Invalid")) {
//				response = "http://localhost:8082/reset-password?token=" + response;
//			}
//			return response;
//		}

		@PostMapping("/reset-password")
		public String resetPassword(@RequestBody ResetPasswordDto resetPasswordDto) {
			//if (PasswordValidator.isValid(resetPasswordDto.getPassword())) {
			return userDetailsService.resetPassword(resetPasswordDto.getToken(), passwordEncoder.encode(resetPasswordDto.getPassword())); //,resetPasswordDto.getConfirmPassword()
		}
			 
    	
		@PreAuthorize("hasRole('getPaginatedJobs')")
		@GetMapping("/jobs")
	    public List<Job> getPaginatedJobs(@RequestParam(required = false,defaultValue = "0")  int pageNo, 
	    		@RequestParam(required = false,defaultValue = "10") int pageSize) {

	        return jobService.findPaginated(pageNo, pageSize);
	    }
		
		@PreAuthorize("hasRole('applyToJob')")
		@PostMapping("/apply")
		public ResponseEntity<?> applyToJob(@Valid @RequestBody JobApplicationDto jobApplicationDto ) {
			
//			if(jobRepo.findJobById(jobApplicationDto.getJobId()) != null 
//					&& userRepository.findUserById(jobApplicationDto.getJwtUserId()) != null) {
//				
//				return new ResponseEntity(new ApiResponse(false, "this job has already been applied"),
//		                   HttpStatus.BAD_REQUEST); 
//			
//		} else {
		jobApplicationService.applyToJob(jobApplicationDto);
		return new ResponseEntity<>("Job Applied Successfully", HttpStatus.CREATED);
			}
//		}
		
			
		
		@Transactional
		//@RequestMapping(value = "/logout",method = RequestMethod.POST)
		@PostMapping("/signout")
		public ResponseEntity<?> logoutUser(@RequestHeader("Authorization") String token) throws Exception { //, HttpServletRequest request

			loggerService.logout(token);//, ((JwtUser) request.getAttribute("userData")).getId(), ((JwtUser) request.getAttribute("userData")).getEmail());
			return new ResponseEntity<>(new ErrorResponseDto("Logout Successfully", "logoutSuccess"), HttpStatus.OK);

}
		@PostMapping("/sendMail")
		public ResponseEntity<?> sendMail(@RequestBody JwtUser user) throws MessagingException{
			//JwtUser jwtuser= this.userRepository.findUserByEmail(user.getEmail());
			this.jobApplicationService.sendSimpleMessage(user.getEmail(), "subject", user, "Application successful");
			return ResponseEntity
					.ok(new SuccessResponseDto("email sent","email sent to user", user.getEmail()));


			//return new ResponseEntity<>("email sent to user", HttpStatus.OK);
		}
		
		 @PostMapping("/forgot")
		 public ResponseEntity<?> forgotPassword(@RequestParam String email, HttpServletRequest request){
			 try {
				 JwtUser user= this.userRepository.findByEmail(email);
				 String response = userDetailsService.forgotPassword(email);
				 //final String token= jwtUtil.generateToken(user);
				 //final String url = "token to reset password is" + token;
				// this.jobApplicationService.sendSimpleMessage(email, response, user, url);
				 this.jobApplicationService.sendSimpleMessage(email,"reset password token" , user, response);
				 return ResponseEntity
							.ok(new SuccessResponseDto("token sent to user email", "token send succesfully", user.getEmail()));


			 }catch (Exception e) {

					return ResponseEntity.ok(new ErrorResponseDto("User not found", "Sorry !!"));


		 }
		
		}}
