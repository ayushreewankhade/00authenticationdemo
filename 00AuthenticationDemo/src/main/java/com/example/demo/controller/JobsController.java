package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.JobDto;
import com.example.demo.service.JobService;

@RestController
public class JobsController {
	
	@Autowired
	JobService jobService;


	@PreAuthorize("hasRole('job')")
	@PostMapping("/postJob")
	public ResponseEntity<Object> job(@RequestBody JobDto jobDto) {
		//System.out.println(jobDto);
		jobService.postJob(jobDto);
		return new ResponseEntity<>("Job Posted Successfully", HttpStatus.OK);
	}
	
}
