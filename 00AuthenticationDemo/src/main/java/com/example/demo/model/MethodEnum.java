package com.example.demo.model;

public enum MethodEnum {
	GET, POST, PUT, DELETE, PATCH, OPTION, OTHER
}
