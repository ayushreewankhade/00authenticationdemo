package com.example.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Recruiter {

	@Id
	@Column(name = "recruiter_id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "recruiter_seq")
	//@SequenceGenerator(name = "recruiter_seq", sequenceName = "recruiter_seq", allocationSize = 1)
	private Long id;

	@Email
	private String email;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//private String firstName;
	//private String lastName;
	
	private String password;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "task.user", cascade = CascadeType.ALL)
	@JsonBackReference
	List<UserRole> userRole;

	
	@OneToMany(mappedBy = "postedBy", targetEntity = Job.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH, CascadeType.DETACH })
	private List<Job> postedJobs;

	public Recruiter() {
		super();
	}

	public Recruiter(String email, List<Job> postedJobs, String password) {
		super();
		this.email = email;
		//this.firstName = firstName;
		//this.lastName = lastName;
		this.postedJobs = postedJobs;
		this.password = password;
	}

	//public String getFirstName() {
	//	return firstName;
	//}

	public Long getId() {
		return id;
	}

	//public String getLastName() {
	//	return lastName;
	//}

	public List<Job> getPostedJobs() {
		return postedJobs;
	}


	//public void setFirstName(String firstName) {
	//	this.firstName = firstName;
	//}

	
	public void setId(Long id) {
		this.id = id;
	}

	//public void setLastName(String lastName) {
	//	this.lastName = lastName;
	//s}

	public void setPostedJobs(List<Job> postedJobs) {
		this.postedJobs = postedJobs;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
