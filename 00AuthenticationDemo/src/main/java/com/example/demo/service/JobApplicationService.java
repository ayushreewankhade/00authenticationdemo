package com.example.demo.service;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.example.demo.dto.JobApplicationDto;
import com.example.demo.dto.LoginRequest;
import com.example.demo.model.Job;
import com.example.demo.model.JobApplication;
import com.example.demo.model.JwtUser;
import com.example.demo.model.LoggerEntity;
import com.example.demo.repo.JobApplicationRepo;
import com.example.demo.repo.JobRepo;
import com.example.demo.repo.JwtUserRepository;

@Service
public class JobApplicationService {
	
	@Autowired
	JobRepo jobRepo;
	
	@Autowired
	JwtUserRepository jwtUserRepository;
	
	@Autowired
	JobApplicationRepo jobApplicationRepo;
	
	@Autowired
	private JavaMailSender javaMailSender;

	public JobApplication applyToJob(JobApplicationDto jobApplicationDto) {
		JobApplication jobApplication = new JobApplication();
		if ((jobApplicationDto.getJwtUserId() != null)
				|| jobApplicationDto.getJobId() != null) {
			
			jobApplication.setJwtUser(jwtUserRepository.findById(jobApplicationDto.getJwtUserId()).get());
			jobApplication.setJob(jobRepo.findById(jobApplicationDto.getJobId()).get());
		}
			return jobApplicationRepo.save(jobApplication);
			
		// else {
			//throw new InvalidJobApplicationException();
		//}
	}
	
	 public List<JobApplication> findPaginatedJobsApplied(int pageNo, int pageSize, String username) {

		 	JwtUser user = jwtUserRepository.findByEmail(username);
	        Pageable paging = PageRequest.of(pageNo, pageSize);
	        Page<JobApplication> pagedResult = jobApplicationRepo.findByJwtUserId(paging, user.getId());

	        return pagedResult.toList();
	    }

	 public void sendSimpleMessage(String emailTo, String subject, JwtUser user,String text) throws MessagingException {

			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);

			helper.setFrom("ayushreewankhade@gmail.com");
			helper.setTo(user.getEmail());
			helper.setSubject(subject);
			helper.setText(text,true);
			javaMailSender.send(message);
		}

}
