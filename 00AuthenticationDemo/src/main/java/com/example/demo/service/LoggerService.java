package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.LoggerDto;
import com.example.demo.model.JwtUser;
import com.example.demo.model.LoggerEntity;
import com.example.demo.repo.LoggerRepository;

@Service
public class LoggerService {

	@Autowired
	private LoggerRepository loggerRepository;

	
	public void createLogger(LoggerDto loggerDto, JwtUser jwtuser) {
		LoggerEntity logger=new LoggerEntity();
		//logger.setId(user);
		logger.setToken(loggerDto.getToken());
		logger.setExpireAt(loggerDto.getExpireAt());
		loggerRepository.save(logger);

	}
	
     public void logout(String token) {
		
		final String token1=token.substring(7);

		loggerRepository.removeByToken(token);


	}
    //@Transactional
	//public void logout(String token, Integer userId, String email) {

		//final String userToken = token;
		//cache.removeKeyFromCache(userToken);
		//cache.removeKeyFromCache(userId + "permission");
		//cache.removeKeyFromCache(email);
		//loggerRepository.removeByToken(userToken);

	//}
}
