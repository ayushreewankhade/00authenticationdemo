package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.PermissionRequestDto;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.PermissionEntity;
import com.example.demo.repo.PermissionRepository;


@Service
public class PermissionServiceImpl  {

	@Autowired
	private PermissionRepository permissionRepository;
	
	

	
	public void addPermission(PermissionRequestDto permissionRequestDto) {
	
		
		PermissionEntity permissionEntity=new PermissionEntity();
		permissionEntity.setActionName(permissionRequestDto.getActionName());
		permissionEntity.setBaseUrl(permissionRequestDto.getBaseUrl());
		permissionEntity.setDiscription(permissionRequestDto.getDescription());
		permissionEntity.setMethod(permissionRequestDto.getMethod());
		permissionEntity.setPath(permissionRequestDto.getPath());
		permissionRepository.save(permissionEntity);
		return;
	}

	public void editPermission(PermissionRequestDto permissionRequestDto, int id) {
		PermissionEntity permissionEntity=this.permissionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("role Not found "));
		
		permissionEntity.setActionName(permissionRequestDto.getActionName());
		permissionEntity.setBaseUrl(permissionRequestDto.getBaseUrl());
		permissionEntity.setDiscription(permissionRequestDto.getDescription());
		permissionEntity.setMethod(permissionRequestDto.getMethod());
		permissionEntity.setPath(permissionRequestDto.getPath());
		permissionRepository.save(permissionEntity);
		return;
	}


	public void deletePermission(int id) {
		PermissionEntity permissionEntity2=this.permissionRepository.findById(id).orElseThrow();
	this.permissionRepository.delete(permissionEntity2);
		
	}
	
	
	

}
