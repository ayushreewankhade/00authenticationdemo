package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.RoleDto;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.RoleEntity;
import com.example.demo.repo.JwtUserRepository;
import com.example.demo.repo.RoleRepo;

@Transactional
@Service("roleServiceImpl")
public class RoleServiceImpl {



	@Autowired
	RoleRepo roleReporsitory;
	@Autowired
	JwtUserRepository userRepo;


	@Autowired
	ModelMapper modelMapper;


	
	public RoleEntity addRoles(RoleDto roleDto) {
		RoleEntity roleEntity=new RoleEntity();
		roleEntity.setRoleName(roleDto.getRoleName());
		return roleReporsitory.save(roleEntity);


	}
	//getting  all roles
	
	public List<RoleDto> getAllRoles() {
		List<RoleEntity> roles=this.roleReporsitory.findAll();
		List<RoleDto> saveRoles=roles.stream().map(e -> this.modelMapper.map(e, RoleDto.class)).collect(Collectors.toList());
		return saveRoles;
	}
	
	
	public void updateRoles(RoleDto roleDto, int id) {
		RoleEntity  role=this.roleReporsitory.findById(id).orElseThrow(() -> new ResourceNotFoundException("Role NOT Found "+id));
		role.setRoleName(roleDto.getRoleName());
		roleReporsitory.save(role);

	}


	public void deletedRoles(Integer id) {
		this.roleReporsitory.findById(id)
		.orElseThrow( () -> new ResourceNotFoundException("role not found"+id));
		this.roleReporsitory.deleteById(id);

	}


	public RoleDto getRoleById(Integer id) {
		RoleEntity roleEntity=this.roleReporsitory.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("ROLE NOT FOUND WITH ID"+id));
		return this.modelMapper.map(roleEntity, RoleDto.class);
	}
}
