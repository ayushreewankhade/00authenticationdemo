
package com.example.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.Recruiter;
import com.example.demo.repo.RecruiterRepo;

@Service
public class RecruiterService implements UserDetailsService{

	@Autowired
	RecruiterRepo recruiterRepo;
	
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Recruiter recruiter = recruiterRepo.findUserByEmail(email);
        if (recruiter == null) {
            throw new UsernameNotFoundException("email Not found" + email);
        }
        return new User(recruiter.getEmail(), recruiter.getPassword(), new ArrayList<>());
    
}}
