package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.AssignRole;
import com.example.demo.model.JwtUser;
import com.example.demo.model.RoleEntity;
import com.example.demo.model.UserRole;
import com.example.demo.model.UserRoleId;
import com.example.demo.repo.JwtUserRepository;
import com.example.demo.repo.RoleRepo;
import com.example.demo.repo.UserRoleRepo;

@Service
public class UserRoleServiceImpl  {

	@Autowired
	private JwtUserRepository userRepo;


	@Autowired
	private UserRoleRepo userRoleRepo;

	@Autowired
	private RoleRepo roleReporsitory;
	@Autowired
	ModelMapper mapper;

	
	public void addUserToRole(AssignRole assignRole) {

			ArrayList<UserRole>roles=new ArrayList<>();
			JwtUser user=this.userRepo.findById(assignRole.getUserId()).orElseThrow(() -> new ResourceNotFoundException("User NOT Found "));
			RoleEntity role=this.roleReporsitory.findById(assignRole.getRoleId()).orElseThrow(() -> new ResourceNotFoundException("ROLE NOT Found "));
			UserRoleId uri = new UserRoleId(user, role);
			UserRole ure = new UserRole();
			ure.setTask(uri);
			roles.add(ure);
			userRoleRepo.saveAll(roles);
		
	}

	
	public void updateUserRoles(AssignRole assignRole){
		JwtUser user=this.userRepo.findById(assignRole.getUserId()).orElseThrow();
		RoleEntity role=this.roleReporsitory.findById(assignRole.getRoleId()).orElseThrow();
		UserRoleId uri = new UserRoleId(user, role);
		UserRole ure = new UserRole();
		ure.setTask(uri);
		
		userRoleRepo.updateUserRoles(user.getId(), role.getId());
	}

	
	public List<UserRole> getAllUserRols1() {

		List<UserRole >list=this.userRoleRepo.findAll();
		//List<User> user= this.userRepo.findAll();

		return list;

	}

	
	public UserRole deleteUserRoles(AssignRole assignRole) {
		
		JwtUser user =this.userRepo.findById(assignRole.getUserId()).orElseThrow(() -> new ResourceNotFoundException("User NOT Found "));
		RoleEntity entity =this.roleReporsitory.findById(assignRole.getRoleId()).orElseThrow(() -> new ResourceNotFoundException("Role NOT Found "));
//		User user =this.userRepo.findByEmail(assignRole.getEmail());
//	RoleEntity entity =this.roleReporsitory.findByName(assignRole.getRoleName());		
	UserRoleId id=new UserRoleId(user, entity);
		UserRole roleEntity=new UserRole();
		roleEntity.setTask(id);
//	this.userRoleRepo.deleteRole(user.getId(),entity.getId());
	this.userRoleRepo.delete(roleEntity);
		return roleEntity;
		
	


}

	
	
	
}
