package com.example.demo.dto;

import javax.validation.constraints.NotNull;

public class JobApplicationDto {
	@NotNull
	private Long jobId;
	
	private Integer jwtUserId;
	
	public JobApplicationDto() {
		super();
	}

	public JobApplicationDto(Long jobId, String coverLetter, Integer jwtUserId) {
		super();
		this.jobId = jobId;
		this.jwtUserId = jwtUserId;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Integer getJwtUserId() {
		return jwtUserId;
	}

	public void setJwtUserId(Integer jwtUserId) {
		this.jwtUserId = jwtUserId;
	}

	@Override
	public String toString() {
		return "JobApplicationDto [jobId=" + jobId + ", jwtUserId=" + jwtUserId + "]";
	}

	
}
