package com.example.demo.dto;

public class JobDto {

	//private long jwtUserid = 2L;
	private Long id;

	private String jobTitle;

	private String jobDescription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public JobDto(Long id, String jobTitle, String jobDescription) {
		super();
		this.id = id;
		this.jobTitle = jobTitle;
		this.jobDescription = jobDescription;
	}

	public JobDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "JobDto [id=" + id + ", jobTitle=" + jobTitle + ", jobDescription=" + jobDescription + "]";
	}
	
}
