package com.example.demo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ChangePasswordDto {

	@NotBlank
	@NotEmpty
	@NotNull
	private String password;

	@NotBlank
	@NotEmpty
	@NotNull
	private String newPassword;
	
	@NotBlank
	@NotEmpty
	@NotNull
	private String confPassword;

	public String getPassword() {

		return password;

	}

	public void setPassword(String password) {

		this.password = password;

	}

	public String getNewPassword() {

		return newPassword;

	}

	public void setNewPassword(String newPassword) {

		this.newPassword = newPassword;

	}

	public String getConfPassword() {
		return confPassword;
	}

	public void setConfPassword(String confPassword) {
		this.confPassword = confPassword;
	}
	
	
	

}
