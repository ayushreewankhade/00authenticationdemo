package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.LoggerEntity;

@Repository
public interface LoggerRepository  extends JpaRepository<LoggerEntity, Integer>{
	
	void  removeByToken(String token);
	
	LoggerEntity findByToken(String token);
}
	
	

