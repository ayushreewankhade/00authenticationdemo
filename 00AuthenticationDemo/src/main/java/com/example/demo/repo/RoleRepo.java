package com.example.demo.repo;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.example.demo.model.RoleEntity;


@Repository
@EnableJpaRepositories
public interface RoleRepo extends JpaRepository<RoleEntity, Integer>{
//@Transactional
//@Modifying(clearAutomatically =  true)
	@Query("SELECT r FROM RoleEntity r WHERE r.roleName = :roleName")
	RoleEntity findByName(String roleName);
	
	ArrayList<RoleEntity> findBy();
	




}
