package com.example.demo.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.JobApplicationDto;
import com.example.demo.dto.LoginRequest;
import com.example.demo.model.JobApplication;

@Repository
public interface JobApplicationRepo extends JpaRepository<JobApplication,Long>{

	Page<JobApplication> findByJwtUserId(Pageable paging, Integer id);

	//JobApplication save(JobApplication jobApplication);
	
	boolean findJobById(boolean b);

	//boolean findByJobId(boolean b);

}
