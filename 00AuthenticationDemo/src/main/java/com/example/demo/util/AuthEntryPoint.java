package com.example.demo.util;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class AuthEntryPoint implements AuthenticationEntryPoint,Serializable{


	private static final long serialVersionUID = -7858869558953243875L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		
		//response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
	
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.getOutputStream().println("{\r\n" + "	\"message\": \"Unauthorized\",\r\n" + "	\"msgKey\": \"unauthorized\"\r\n" + "}");
//		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//		response.getOutputStream().println("{\r\n" + "	\"message\": \"Token Expired\",\r\n" + "	\"msgKey\": \"token expired\"\r\n" + "}");

	}}

//@Override
//public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
//                     AuthenticationException e) throws IOException, ServletException {
//    ObjectMapper mapper = new ObjectMapper();
//    ErrorDetails errorDetails = ErrorDetails.builder()
//            .details(String.valueOf(e.getClass()))
//            .message("JWT has expired")
//            .timestamp(DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss", Locale.ENGLISH)
//                    .format(LocalDateTime.now()))
//            .build();
//    httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
//    httpServletResponse.setContentType("application/json");
//    httpServletResponse.setCharacterEncoding("UTF-8");
//    httpServletResponse.getWriter().write(mapper.writeValueAsString(errorDetails));
