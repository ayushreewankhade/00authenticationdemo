package com.example.demo.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.example.demo.exception.JwtTokenMalformedException;
import com.example.demo.exception.JwtTokenMissingException;
import com.example.demo.model.JwtUser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {

	
	private String SECRET_KEY = "secret";

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(JwtUser user) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, user.getEmail());
    }

    public String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 900000))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails)throws JwtTokenMalformedException, JwtTokenMissingException {
    	try {
    	final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    
        //Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
    } catch (SignatureException e) {
        throw new JwtTokenMalformedException("Invalid JWT signature");
    } catch (MalformedJwtException e) {
        throw new JwtTokenMalformedException("Invalid JWT token");
    } catch (ExpiredJwtException e) {
        throw new JwtTokenMalformedException("Expired JWT token");
    } catch (UnsupportedJwtException e) {
        throw new JwtTokenMalformedException("Unsupported JWT token");
    } catch (IllegalArgumentException e) {
        throw new JwtTokenMissingException("JWT claims string is empty.");
    }}

}
