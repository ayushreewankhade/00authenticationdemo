package com.example.demo.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class Pagination {
	public Pagination() {
		super();

	}

	public Pageable getPagination(int pageNumber, int pageSize) {
		return PageRequest.of(pageNumber -1, pageSize);
	}

}
